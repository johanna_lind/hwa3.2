// Kasutatud kirjandus:
// http://enos.itcollege.ee/%7ejpoial/algoritmid/adt.html

public class DoubleStack {

    public static void main (String[] argum) {

        //DoubleStack m = new DoubleStack();
        //String s = "35. 10. -3. + / ";

        //System.out.println(m.interpret(s));
        //m.pop();
        //m.push (2.);
        //m.push (5.);
        //double k = m.tos();
        //double k2 = m.pop();
        //System.out.println(k);
        //System.out.println(k2);
        //double k3 = m.pop();
        //System.out.println (k3);

        String s = "2. 5. - 4. +";
        String n = "2. 5. 9. ROT + SWAP -";
        System.out.println(interpret(n));

    }

    private double[] magasin;  // magasin ise massiivina
    private int SP;  // tipu indeks

    DoubleStack() {  // konstruktor
        magasin = new double[10];
        SP = -1;
    }


    public DoubleStack(double[] magasin, int SP) {
        this.magasin = magasin;
        this.SP = SP;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        //return this;

        DoubleStack tmp = new DoubleStack();
        tmp.SP = SP;
        if (SP >= 0)
            for (int i=0; i<=SP; i++)
                tmp.magasin [i] = magasin [i];
        return tmp;
    }

    public boolean stEmpty() {
        return (SP== -1);
    }

    public boolean stOver() {  // on lõhki, ületäitunud
        return ((SP + 1) >= magasin.length);
    }

    public void push (double a) {
        if (stOver())
            throw new IndexOutOfBoundsException ("magasini yletaitumine");
        SP += 1; // increment
        magasin [SP] = a;
    }

    public double pop() {
        if (stEmpty())
            throw new IndexOutOfBoundsException ("magasini alataitumine");
        double tmp = magasin [SP];
        SP -= 1; // decrement
        return tmp;
        //return 0.; //
    }

    public void op (String s) {
        double op2 = pop();
        double op1 = pop();
        if (s.equals ("+")) push (op1 + op2);
        if (s.equals ("-")) push (op1 - op2);
        if (s.equals ("*")) push (op1 * op2);
        if (s.equals ("/")) push (op1 / op2);
    }

    public double tos() {
        if (stEmpty())
            throw new IndexOutOfBoundsException ("magasini alataitumine");
        return magasin [SP];
    }

    @Override
    public boolean equals (Object o) {
        if (((DoubleStack)o).SP != SP) return false;
        if (stEmpty()) return true; // teine ka tyhi!
        for (int i=0; i<=SP; i++)
            if (((DoubleStack)o).magasin[i] != magasin [i]) return false;
        return true;
    }

    @Override
    public String toString() {

        if (stEmpty()) return "Tyhi";
        StringBuffer b = new StringBuffer();
        for (int i=0; i<=SP; i++)
            b.append (String.valueOf (magasin [i]) + " ");
        return b.toString();
    }

    public static double interpret (String pol) {  // s = "2. 5. -";
        DoubleStack test = new DoubleStack();

        String[] help= pol.split(" ");

        for (int i = 0; i < help.length; i++) {
            try {
                double number = Double.parseDouble(help[i].replace(".", ""));
                test.push(number);
            } catch (NumberFormatException e) {
                if ((help[i].trim()).equals("+") || (help[i].trim()).equals("-") || (help[i].trim()).equals("/") || (help[i].trim()).equals("*")) {
                    test.op(help[i].trim());
                } else if (help[i].equals("SWAP")) {
                    Double swap1 = test.pop();
                    Double swap2 = test.pop();
                    test.push(swap1);
                    test.push(swap2);

                } else if (help[i].equals("ROT")) {
                    Double swap1 = test.pop();
                    Double swap2 = test.pop();
                    Double swap3 = test.pop();

                    test.push(swap2);
                    test.push(swap1);
                    test.push(swap3);
                } else {
                    System.out.println("Character does not suit");
                }
            }
        }
        double vastus = test.pop();
        try {
            test.pop();
            throw new RuntimeException("Magasin ei ole lopuni kasutatud");
        } catch (IndexOutOfBoundsException e) {
            return vastus;
        }
    }
}
